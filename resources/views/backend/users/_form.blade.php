<div class="form-group @if ($errors->has('gelaran')) has-error @endif">
        {!! Form::label('gelaran', 'Gelaran') !!} {!! Form::text('gelaran', null, ['class' => 'form-control']) !!}
        @if ($errors->has('gelaran'))
        <p class="help-block">{{ $errors->first('gelaran') }}</p> @endif
    </div>
<!-- Name Form Input -->
<div class="form-group @if ($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Nama Penuh') !!} {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @if ($errors->has('name'))
    <p class="help-block">{{ $errors->first('name') }}</p> @endif
</div>

<!-- email Form Input -->
<div class="form-group @if ($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'E-mel') !!} {!! Form::text('email', null, ['class' => 'form-control'])
    !!} @if ($errors->has('email'))
    <p class="help-block">{{ $errors->first('email') }}</p> @endif
</div>
<!-- password Form Input -->
<div class="form-group @if ($errors->has('password')) has-error @endif">
    {!! Form::label('password', 'Kata Laluan') !!} {!! Form::password('password', ['class' => 'form-control'])
    !!} @if ($errors->has('password'))
    <p class="help-block">{{ $errors->first('password') }}</p> @endif
</div>
<div class="form-group @if ($errors->has('iclama')) has-error @endif">
    {!! Form::label('name', 'No. IC Lama (Jika Ada)') !!} {!! Form::text('iclama', null, ['class' => 'form-control']) !!}
    @if ($errors->has('iclama'))
    <p class="help-block">{{ $errors->first('iclama') }}</p> @endif
</div>
<div class="form-group @if ($errors->has('icbaru')) has-error @endif">
    {!! Form::label('name', 'No. IC Baru') !!} {!! Form::text('icbaru', null, ['class' => 'form-control']) !!}
    @if ($errors->has('icbaru'))
    <p class="help-block">{{ $errors->first('icbaru') }}</p> @endif
</div>
<div class="form-group @if ($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Tarikh Lahir') !!} {!! Form::text('dob', null, ['class' => 'form-control']) !!}
    @if ($errors->has('dob'))
    <p class="help-block">{{ $errors->first('dob') }}</p> @endif
</div>
<div class="form-group @if ($errors->has('alamat')) has-error @endif">
    {!! Form::label('alamat', 'Alamat') !!} 
    {!! Form::text('alamat1', null, ['class' => 'form-control']) !!}
    {!! Form::text('alamat2', null, ['class' => 'form-control']) !!}
    @if ($errors->has('alamat'))
    <p class="help-block">{{ $errors->first('alamat') }}</p> @endif
</div>
<div class="form-group">
<div class="form-inline">
        {!! Form::text('alamat1', null, ['class' => 'form-control mb-2 mr-sm-2','placeholder'=>'Poskod']) !!}
        {!! Form::text('alamat1', null, ['class' => 'form-control mb-2 mr-sm-2','placeholder'=>'Bandar']) !!}
        {!! Form::text('alamat1', null, ['class' => 'form-control mb-2 mr-sm-2','placeholder'=>'Negeri']) !!}
</div>
</div>
<div class="form-group @if ($errors->has('file')) has-error @endif">
        <label for="" class="">Muat Naik Salinan IC</label>
        <br>
        <input type="file">    
        @if ($errors->has('ic'))
        <p class="help-block">{{ $errors->first('ic') }}</p> 
        @endif
</div>
<div class="form-group @if ($errors->has('name')) has-error @endif">
    @if ($errors->has('name'))
    <p class="help-block">{{ $errors->first('name') }}</p> @endif
</div>
@role('Administrator')
<!-- Permissions -->
@if(isset($user)) @include('shared._permissions', ['closed' => 'true', 'model' => $user ]) @endif @endrole @include('asset-partials.selectize')