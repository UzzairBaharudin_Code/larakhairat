@extends('backend.master')

@section('content')
<div class="page-header">
    <h1 class="page-title">
      Laman Utama
    </h1>
</div>
<div class="row row-cards">
                                               
              <div class="col-sm-6 col-lg-4">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-primary mr-3">
                      <i class="fe fe-users"></i>
                    </span>
                    <div>
                    <h4 class="m-0"><a href="#">700 <small>Ahli</small></a></h4>
                      <small class="text-muted">550 Ahli Aktif</small>
                    </div>
                  </div>
                </div>
              </div>
      
              <div class="col-sm-6 col-lg-4">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-primary mr-3">
                      <i class="fe fe-credit-card"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)">700 <small>Ahli Dengan IC</small></a></h4>                      
                    </div>
                  </div>
                </div>
              </div>
              @can('view_users')
              <div class="col-sm-6 col-lg-4">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-primary mr-3">
                      <i class="fe fe-user"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="#">{{$users->count()}} <small>Ahli Baru</small></a></h4>
                      <small class="text-muted">2 orang ahli telah mendaftar pada hari ini</small>
                    </div>
                  </div>
                </div>
              </div>
              @endcan              
            </div>
            <div class="row row-cards">
                <div class="col-6 col-sm-3 col-lg-3">
                  <div class="card">
                    <div class="card-body p-3 text-center">
                      <div class="text-right text-green">                        
                      </div>
                      <div class="h1 m-0">43</div>
                      <div class="text-muted mb-4">Tuntutan vs Kutipan Tahun Ini</div>                      
                    </div>
                  </div>
                </div>
                <div class="col-6 col-sm-3 col-lg-3">
                  <div class="card">
                    <div class="card-body p-3 text-center">                      
                      <div class="h1 m-0">17</div>
                      <div class="text-muted mb-3">Tuntutan vs Kutipan Keseluruhan</div>
                    </div>
                  </div>
                </div>
                <div class="col-6 col-sm-3 col-lg-3">
                  <div class="card">
                    <div class="card-body p-3 text-center">                      
                      <div class="h1 m-0">7</div>
                      <div class="text-muted mb-4">Kutipan vs Tunggakan Tahun Ini</div>
                    </div>
                  </div>
                </div>
                <div class="col-6 col-sm-3 col-lg-3">
                  <div class="card">
                    <div class="card-body p-3 text-center">                      
                      <div class="h1 m-0">10</div>
                      <div class="text-muted mb-4">Kutipan vs Tunggakan Keseluruhan</div>
                    </div>
                  </div>
                </div>                                
              </div>
@endsection