<div class="header py-4">
    <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="{{route('home')}}">
                <img src="{{asset('img/logo.png')}}" class="header-brand-img" alt="brillante logo">
            </a>
            <div class="d-flex order-lg-2 ml-auto">
                {{-- <div class="nav-item d-none d-md-flex">
                    <a href="{{route('tickets.create')}}" class="btn btn-sm btn-outline-primary">
                        <i class="fe fe-plus-circle"></i> New Ticket</a>
                </div> --}}
                <div class="dropdown d-none d-md-flex">
                    <a class="nav-link icon" data-toggle="dropdown">
                        <i class="fe fe-bell"></i>
                        <span class="nav-unread"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a href="#" class="dropdown-item d-flex">
                            <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/male/41.jpg)"></span>
                            <div>
                                <strong>Nathan</strong> pushed new commit: Fix page load performance issue.
                                <div class="small text-muted">10 minutes ago</div>
                            </div>
                        </a>
                        <a href="#" class="dropdown-item d-flex">
                            <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/female/1.jpg)"></span>
                            <div>
                                <strong>Alice</strong> started new task: Tabler UI design.
                                <div class="small text-muted">1 hour ago</div>
                            </div>
                        </a>
                        <a href="#" class="dropdown-item d-flex">
                            <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/female/18.jpg)"></span>
                            <div>
                                <strong>Rose</strong> deployed new version of NodeJS REST Api V3
                                <div class="small text-muted">2 hours ago</div>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item text-center text-muted-dark">Mark all as read</a>
                    </div>
                </div>
                <div class="dropdown">
                    <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                        <span class="avatar" style="background-image: url({{asset(Auth::user()->profile->avatar)}})"></span>
                        <span class="ml-2 d-none d-lg-block">
                            <span class="text-default">{{Auth::user()->name}}</span>
                            <small class="text-muted d-block mt-1">{{Auth::user()->roles()->pluck('name')->first()}} {{-- @if(!empty(Auth::user()->profile->department->name))
                                {{Auth::user()->profile->department->name}} @endif --}}
                            </small>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        @can('edit_profiles')
                        <a class="dropdown-item" href="/myprofile">
                            <i class="dropdown-icon fe fe-user"></i> Profail Saya
                        </a>
                        @endcan
                        <a class="dropdown-item" href="#">
                            <i class="dropdown-icon fe fe-settings"></i> Tetapan
                        </a>                        
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="dropdown-icon fe fe-log-out"></i> {{ __('Log Keluar') }}
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </a>
                    </div>
                </div>
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
            </a>
        </div>
    </div>
</div>
<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3 ml-auto">
                <form class="input-icon my-3 my-lg-0">
                    <input type="search" class="form-control header-search" placeholder="Carian&hellip;" tabindex="1">
                    <div class="input-icon-addon">
                        <i class="fe fe-search"></i>
                    </div>
                </form>
            </div>
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="{{route('home')}}" class="nav-link">
                            <i class="fe fe-home"></i> Laman Utama</a>
                    </li>                    
                    @role('Administrator') {{-- Laravel-permission blade helper --}}
                    <li class="nav-item dropdown">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-dollar-sign"></i> Kewangan</a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                        <a href="{{route('invois.yuranbaru')}}" class="dropdown-item">Invois Yuran Baru</a>
                            <a href="{{route('invois.rekodyuran')}}" class="dropdown-item">Urus Rekod Yuran</a>
                            <a href="{{route('invois.rekodyuran')}}" class="dropdown-item">Proses Bayaran Yuran</a>
                            <a href="{{route('invois.rekodyuran')}}" class="dropdown-item">Proses Tuntutan</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-mail"></i> Surat</a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="{{route('surats.index')}}" class="dropdown-item">Senarai Surat</a>
                            <a href="{{route('surats.janapekeliling')}}" class="dropdown-item">Surat Pekeliling</a>
                            <a href="{{route('surats.janaperingatan')}}" class="dropdown-item">Surat Peringatan</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                            <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                                <i class="fe fe-file-text"></i> Laporan</a>
                            <div class="dropdown-menu dropdown-menu-arrow">
                                <h5 class="dropdown-item font-weight-bold">Ahli</h5>                                              
                                <a href="#" class="dropdown-item">Senarai Ahli Beserta Bayaran</a>
                                <div class="dropdown-divider"></div>
                                <h5 class="dropdown-item font-weight-bold">Yuran</h5>                                
                                <a href="#" class="dropdown-item">Laporan Tunggakan Yuran Ahli</a>
                                <a href="#" class="dropdown-item">Laporan Pembayaran Yuran Ahli</a>
                                <a href="#" class="dropdown-item">Laporan Tuntutan Dibuat Ahli</a>
                                <div class="dropdown-divider"></div>
                                <h5 class="dropdown-item font-weight-bold">Bendahari</h5>                                
                                <a href="#" class="dropdown-item">Laporan Pembayaran Yuran</a>
                            </div>
                        </li>
                        <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="fe fe-activity"></i> Audit Aktiviti</a>
                        </li>
                    @endrole 
                    @role('Administrator') {{-- Laravel-permission blade helper --}}
                    <li class="nav-item dropdown">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
                            <i class="fe fe-shield"></i> Pentadbiran</a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="{{route('users.index')}}" class="dropdown-item">Senarai Ahli</a>
                            <a href="{{route('roles.index')}}" class="dropdown-item">Roles & Permissions</a>
                        </div>
                    </li>
                    @endrole
                </ul>
            </div>
        </div>
    </div>
</div>