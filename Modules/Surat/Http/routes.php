<?php

Route::group(['middleware' => 'web', 'prefix' => 'backend', 'namespace' => 'Modules\Surat\Http\Controllers'], function()
{
    Route::resource('surats', 'SuratsController');
    Route::get('/jana/pekeliling', 'SuratsController@createSuratPekeliling')->name('surats.janapekeliling');
    Route::get('/jana/peringatan', 'SuratsController@createSuratPeringatan')->name('surats.janaperingatan');
});
