@extends('backend.master') @section('content')
@card 
@cardHeader @slot('card_title')
<i class="fe fe-mail"></i> Jana Surat Peringatan @endslot
@cardOptions

        @endcardOptions 
        @endcardHeader 
        @cardBody
        <form action="" class="AVAST_PAM_nonloginform">                
                <div class="form-group">
                  <div class="row align-items-center">
                    <label class="col-sm-2">Tajuk Surat:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
                <textarea rows="10" class="form-control" placeholder="Isi kandungan surat"></textarea>
                <div class="btn-list mt-4 text-right">
                  <button type="button" class="btn btn-secondary btn-space">Batal</button>
                  <button type="submit" class="btn btn-primary btn-space">Jana Surat</button>
                </div>
              </form>
        @endcardBody
        @endcard
    @endsection