<?php

Route::group(['middleware' => 'web', 'prefix' => 'backend', 'namespace' => 'Modules\Invois\Http\Controllers'], function()
{
    Route::resource('invois', 'InvoisController');
    Route::get('rekod-yuran','InvoisController@urusRekodYuran')->name('invois.rekodyuran');
    Route::get('yuran-baru','InvoisController@createYuranBaru')->name('invois.yuranbaru');
});
