@extends('backend.master') @section('content')
<div class="row">
    <div class="col-8">
        @card @cardHeader @slot('card_title')
        <i class="fe fe-mail"></i> Jana Yuran Baru Khairat @endslot @cardOptions @endcardOptions @endcardHeader @cardBody

        <form action="" class="AVAST_PAM_nonloginform">
            <div class="form-group">
                <label class="">Perkara</label>
                <div class="">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="">Jumlah</label>
                <div class="">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="">Untuk Tahun</label>
                <div class="">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="btn-list mt-4">
                    <button type="button" class="btn btn-secondary btn-space">Keluarkan Invois E-khairat</button>
                    <button type="submit" class="btn btn-primary btn-space">Kembali Ke Laman Utama</button>
                </div>
            </div>
        </form>

        @endcardBody @endcard
    </div>
    <div class="col-4">
        @card @cardHeader @slot('card_title')
        <i class="fe fe-check"></i> Panduan @endslot @cardOptions @endcardOptions @endcardHeader @cardBody
        <ol>
            <li>Pastikan anda masukkan jumlah yuran tahunan yang betul.
            </li>
            <li>Invois akan dikeluarkan kepada semua ahli bila tekan butang 'Keluarkan invoice khairat'.</li>
        </ol>

        @endcardBody @endcard
    </div>
</div>
@endsection