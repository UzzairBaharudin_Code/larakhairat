@extends('backend.master') @section('content') @card @cardHeader @slot('card_title')
<i class="fe fe-mail"></i> Senarai Yuran @endslot @cardOptions
<a class="btn btn-secondary btn-sm">Total: 0 Yuran</a>
    @endcardOptions 
    @endcardHeader 
    @cardBody
<div class="table-responsive">
    @table(['class'=>'table table-vcenter text-nowrap card-table table-striped', 'id'=>'datatable'])
    <thead>
        <th>#</th>
        <th>No. Yuran</th>
        <th>Status</th>
        <th>Dijana Pada</th>
        <th class="text-center">Tindakan</th>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-center">
                <div class="item-action dropdown">
                    <a href="javascript:void(0)" data-toggle="dropdown" class="icon">
                        <button class="btn btn-sm btn-secondary">Tindakan</button>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">

                        <a href="#" class="dropdown-item">
                            <i class="fe fe-eye"></i> Lihat</a>
                        <a href="#" class="dropdown-item">
                            <i class="fe fe-edit"></i> Edit</a>
                        <div class="dropdown-divider"></div>
                        {!! Form::open( ['method' => 'delete', 'style' => 'display: inline', 'onSubmit' => 'return confirm("Teruskan padam surat
                        ini?")']) !!}
                        <button type="submit" class="dropdown-item">
                            <i class="fe fe-trash"></i> Delete
                        </button>
                        {!! Form::close() !!}

                    </div>
                </div>
            </td>


        </tr>

    </tbody>
    @endtable
</div>
<div class="container">

</div>
@endcardBody @endcard @stop @include('asset-partials.datatables')